# RestNG Parent

This is a project template for running multiple web services locally that can be called 
from other local applications. Often fixing an issue requires a change to both an 
application and a web service. This project is set up in such a way to allow you to pull 
RestNG projects down into this project then make and test changes in those webservices and 
a web project located elsewhere on the developers machine that consume those services.

## Getting Started

### STEP 1 Pull and prepare web service

Pull down the web service(s) you want to run into the /services folder of this project

Add or uncomment a line referencing the location of the webservice in
 - /docker/config/autoloaders.yaml
 - /docker/config/providers.yaml

In the web service code that was pulled find the resource provider file:

Get the module name from /service/[web service folder]/src/Resources/[service]ResourceProvider.php

find middleware section 	:
		
		'middleware' => array(
    				'authenticate' => array(),
    				'authorize' => array(
        				array(
            				'type' => 'self',
            				'param' => 'muid',
        				),
        				array(
            				'application' => 'WebServices',
            				'module' => ‘[Web Service Module Name]’,
            				'key' => 'view'
        				),
    			)
		)

If middleware is empty, then that mean the webservice does not need to authorize users and the next step can be skipped.
  
Add name to docker/config/authorizations.yaml

	[Web Service Module Name]:
  		view:
    			- bob
  		create:
    			- bob
  		update:
    			- bob
  		delete:
    			- bob

### STEP 2 Create Docker Network

    docker network create nginx-restng


### STEP 3 Update Application to use the docker network and local web services.

If the Application has docker-compose-rest.yml skip to the next step

Create a copy of docker-compose.yml named docker-compose-rest.yml

Append the following to docker-compose-rest.yml
    
    networks:
      laravel:
      restng:
        external: true    


Run the application with the rest specific yml file

    docker-compose -f docker-compose-rest.yml up -d

You should now be able to ssh into that docker container and do a ping/curl request to https://nginx-restng/api/.

Update the app to use for local 
 - Update application code/env so RestNG calls use the docker network host url (https://nginx-restng/api/) 
 - Update application to not verify SSL when calling RestNG webservices


